---
title: "flat_check_data.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# check_primary_color_is_ok
    
```{r function-check_primary_color_is_ok}
#' Check color in a vector 
#' 
#' Check color in vector
#' 
#' @param string vector or character. A color
#' 
#' @return Boolean. TRUE if all color are correct
#' 
#' @export
check_primary_color_is_ok <- function(string) {
  
  
  all_colors_OK <- all(
    string %in% c("Gray", "Cinnamon", "Black", NA)
  )
  
  if (isFALSE(all_colors_OK)) {
    stop("Toutes les couleurs ne sont pas ok")
  }
  
  return(all_colors_OK)
}
```
  
```{r example-check_primary_color_is_ok}
check_primary_color_is_ok(string = c("Gray", "Cinnamon", "Black", NA))
check_primary_color_is_ok(string = c("Gray", "Cinnamon"))
# check_primary_color_is_ok(string = c("Gray", "Blue"))
```
  
```{r tests-check_primary_color_is_ok}
test_that("check_primary_color_is_ok works", {
  expect_true(inherits(check_primary_color_is_ok, "function"))
  
  expect_true(
    object = check_primary_color_is_ok(string = c("Gray", "Cinnamon", "Black", NA))
  )

  expect_true(
    object = check_primary_color_is_ok(string = c("Gray", "Cinnamon", "Black"))
  )
  
  expect_error(
    object = check_primary_color_is_ok(string = c("Gray", "Blue")),
    regexp = "Toutes les couleurs ne sont pas ok"
  )
})
```
  

# check_squirrel_data_integrity()


La fonction 'check_squirrel_data_integrity()' vérifie que les données contiennent une colonne "primary_fur_color", et que cette colonne ne doit contenir que des couleurs autorisées (voir votre fonction check_primary_color_is_ok())
    Arrêter la fonction si ce n’est pas le cas
    Retourner un message si tout est correct

```{r function-check_squirrel_data_integrity()}
#' Title
#' 
#' Description
#' 
#' @return
#' 
#' @export
#' 
check_squirrel_data_integrity <- function(df){
  nom_data <- names(df)
  
  if(isTRUE(any(nom_data == "primary_fur_color"))){
    message("La colonne primary_fur_color est bien présente.")
  } else{
    stop("La colonne attendue 'primary_fur_color' est absente.")
  }
  
  nom_color <- df$primary_fur_color
  
  
  if(isTRUE(all(nom_color %in% c("Gray", "Cinnamon", "Black", NA)))){
    message("Les couleurs de fourrure attendues sont bonnes.")
  } else{
    stop("Une couleur saisie au moins est erronée.")
  }
}
```
  
```{r example-check_squirrel_data_integrity()}
check_squirrel_data_integrity(data_act_squirrels)
#La colonne primary_fur_color est bien présente.
#Les couleurs de fourrure attendues sont bonnes.
```
  
```{r tests-check_squirrel_data_integrity()}
test_that("check_squirrel_data_integrity() works", {
  expect_true(inherits(check_squirrel_data_integrity(), "function")) 
})
```
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(
  flat_file = "dev/flat_check_data.Rmd",
  vignette_name = "Check data"
)
```



<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrelsfabrice

<!-- badges: start -->
<!-- badges: end -->

Le but du package squirrelsfabrice est de comprendre le fonctionnement
et la construction d’un package.

## Installation

Vous pouvez installer mon package en utilisant cette commande :

``` r
install.packages("squirrelsfabrice")
```

## Example

Un exemple de ce que vous permettre mon package, tester la couleur à
saisir :

``` r
library(squirrelsfabrice)
check_primary_color_is_ok("Gray")
#> [1] TRUE
```

## Les fonctions

``` r
check_primary_color_is_ok("Gray")
#> [1] TRUE
get_message_fur_color("Black")
#> We will focus on Black squirrels
```
